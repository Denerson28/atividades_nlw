import express from "express"; //usar o comando yarn add @types/express pra sumir os 3 pontinhos debaixo do express ali, pra instalar as tipagens que ele exige
import { createServer} from "http";
import { Server, Socket } from "socket.io";
import path from "path";

import "./database";
import { routes } from "./routes";

const app = express(); // servidor

app.use(express.static(path.join(__dirname, "..", "public"))); // os ".." significa que voltou uma pasta, estava na src e retornou pra raiz
app.set("views", path.join(__dirname, "..", "public"));
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");

app.get("/pages/client", (request, response) => {
    return response.render("html/client.html");
});

const http = createServer(app); // criando protocolo http
const io = new Server(http); // criando o protocolo de websocket (WS)

io.on("connection", (socket: Socket) => {
    //console.log("se conectou", socket.id);
});

app.use(express.json());

app.use(routes);

/*
* GET = Buscas
* POST = Criação
* PUT = Alteração
* DELETE = Deletar
* PATCH = Alterar uma informação especifica
*/


export { http, io }